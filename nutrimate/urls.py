from django.contrib import admin
from django.urls import path, include

from nutrimate import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/auth/', include('users.urls')),
    path('api/', include('core.urls')),

    path('docs-login/', include('rest_framework.urls')),  # auth links to Browsable API
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
