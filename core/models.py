from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class Diet(models.Model):
    name = models.CharField(max_length=64)
    description = models.TextField(null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='diets')
    proteins = models.DecimalField(max_digits=4, decimal_places=1, help_text='Goal protein value')
    carbs = models.DecimalField(max_digits=4, decimal_places=1, help_text='Goal carbs value')
    fats = models.DecimalField(max_digits=4, decimal_places=1, help_text='Goal fats value')
    calories = models.IntegerField(help_text='Goal calories value')
    date_created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-date_created']

    def __str__(self):
        return f'[{self.pk}] {self.name}'


class Product(models.Model):
    name = models.CharField(max_length=64)
    proteins = models.DecimalField(max_digits=4, decimal_places=1)
    carbs = models.DecimalField(max_digits=4, decimal_places=1)
    fats = models.DecimalField(max_digits=4, decimal_places=1)
    calories = models.IntegerField()
    date_created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['pk']

    def __str__(self):
        return f'[{self.pk}] {self.name}'


class Meal(models.Model):
    name = models.CharField(max_length=64)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='meals')
    diet = models.ForeignKey(Diet, on_delete=models.CASCADE, related_name='meals')
    products = models.ManyToManyField(Product, through='MealHasProduct', related_name='meals')
    date_created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['pk']

    def __str__(self):
        return f'[{self.pk}] {self.name}'


class MealHasProduct(models.Model):
    meal = models.ForeignKey(Meal, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=0)
    date_created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['pk']
        unique_together = ['meal', 'product']

    def __str__(self):
        return f'[{self.pk}] {self.meal.name} - {self.product.name}'
