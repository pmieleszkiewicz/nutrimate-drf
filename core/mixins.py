class MultiSerializerViewSetMixin():
    """
    Mixin allows to use different serializers - depending on action.
    Requires 'serializer_action_classes' field.
    """
    def get_serializer_class(self):
        try:
            return self.serializer_action_classes[self.action]
        except (KeyError, AttributeError):
            return self.serializer_class