# Generated by Django 3.0.5 on 2020-04-12 22:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='mealhasproduct',
            unique_together={('meal', 'product')},
        ),
    ]
