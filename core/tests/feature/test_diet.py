from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model

from core.models import Meal, Diet

User = get_user_model()


class DietTests(APITestCase):
    def setUp(self) -> None:
        self.user = User.objects.create_user("test@example.com", "secret")
        self.diet = Diet.objects.create(name='Diet', user=self.user, proteins=150, carbs=300, fats=100, calories=2700)

    """
    List diet tests
    [GET] /api/diets/
    """
    def test_list_diet_when_not_authenticated(self):
        url = reverse("diet-list")
        response = self.client.get(url, format="json")

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_list_diet(self):
        Diet.objects.create(name='Diet', user=self.user, proteins=150, carbs=300, fats=100, calories=2700)
        Diet.objects.create(name='Diet', user=self.user, proteins=150, carbs=300, fats=100, calories=2700)

        other_user = User.objects.create_user("other@example.com", "secret")
        Diet.objects.create(name='Diet', user=other_user, proteins=150, carbs=300, fats=100, calories=2700)
        Diet.objects.create(name='Diet', user=other_user, proteins=150, carbs=300, fats=100, calories=2700)

        url = reverse("diet-list")
        self.client.force_authenticate(user=self.user)
        response = self.client.get(url, format="json")

        assert response.status_code == status.HTTP_200_OK
        assert len(response.data['results']) == self.user.diets.count()

    """
    Create diet tests
    [POST] /api/diets/
    """
    def test_create_diet_when_not_authenticated(self):
        url = reverse("diet-list")
        data = {}
        response = self.client.post(url, data, format="json")

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_create_diet_invalid_data(self):
        url = reverse("diet-list")
        data = {}
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, data, format="json")

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        self.assertIn('name', response.data)

    def test_create_diet_valid_data(self):
        url = reverse("diet-list")
        data = {'name': 'Mass gaining diet', 'proteins': 150, 'carbs': 300, 'fats': 100, 'calories': 2700}
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, data, format="json")

        assert response.status_code == status.HTTP_201_CREATED

    """
    Update diet tests
    [PUT] /api/diets/<id>
    """
    def test_put_diet_when_not_authenticated(self):
        url = reverse("diet-detail", args={'pk': self.diet.id})
        data = {}
        response = self.client.put(url, data, format="json")

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_put_diet_invalid_data(self):
        url = reverse("diet-detail", kwargs={'pk': self.diet.id})
        data = {}
        self.client.force_authenticate(user=self.user)
        response = self.client.put(url, data, format="json")

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        self.assertIn('name', response.data)

    def test_put_diet_valid_data(self):
        url = reverse("diet-detail", kwargs={'pk': self.diet.id})
        data = {
            'name': 'Edited', 'proteins': 150, 'carbs': 300, 'fats': 100, 'calories': 2700
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.put(url, data, format="json")

        assert response.status_code == status.HTTP_200_OK
        assert response.data['name'] == 'Edited'

    def test_put_diet_when_not_owner(self):
        not_owner = User.objects.create_user("not_owner@example.com", "secret")

        url = reverse("diet-detail", kwargs={'pk': self.diet.id})
        data = {
            'name': 'Edited', 'proteins': 150, 'carbs': 300, 'fats': 100, 'calories': 2700
        }
        self.client.force_authenticate(user=not_owner)
        response = self.client.put(url, data, format="json")

        assert response.status_code == status.HTTP_404_NOT_FOUND

    """
    Update diet tests
    [PATCH] /api/diets/<id>
    """
    def test_patch_meal_when_not_authenticated(self):
        url = reverse("diet-detail", args={'pk': self.diet.id})
        data = {}
        response = self.client.patch(url, data, format="json")

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_patch_meal_valid_data(self):
        url = reverse("diet-detail", kwargs={'pk': self.diet.id})
        data = {
            'name': 'Edited',
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.patch(url, data, format="json")

        assert response.status_code == status.HTTP_200_OK
        assert response.data['name'] == 'Edited'

    def test_patch_diet_when_not_owner(self):
        not_owner = User.objects.create_user("not_owner@example.com", "secret")

        url = reverse("diet-detail", kwargs={'pk': self.diet.id})
        data = {
            'name': 'Edited',
        }
        self.client.force_authenticate(user=not_owner)
        response = self.client.patch(url, data, format="json")

        assert response.status_code == status.HTTP_404_NOT_FOUND

    """
    Retrieve diet tests
    [GET] /api/diet/<id>
    """

    def retrieve_diet_when_not_authenticated(self):
        url = reverse("diet-detail", args={'pk': self.diet.id})
        response = self.client.get(url, format="json")

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_retrieve_diet_valid_data(self):
        url = reverse("diet-detail", kwargs={'pk': self.diet.id})
        self.client.force_authenticate(user=self.user)
        response = self.client.patch(url, format="json")

        assert response.status_code == status.HTTP_200_OK

    def test_retrieve_diet_when_not_owner(self):
        not_owner = User.objects.create_user("not_owner@example.com", "secret")

        url = reverse("diet-detail", kwargs={'pk': self.diet.id})
        self.client.force_authenticate(user=not_owner)
        response = self.client.patch(url, format="json")

        assert response.status_code == status.HTTP_404_NOT_FOUND

    """
    Delete meal tests
    [DELETE] /api/diets/<id>
    """

    def test_delete_diet_when_not_authenticated(self):
        url = reverse("diet-detail", args={'pk': self.diet.id})
        response = self.client.delete(url, format="json")

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_delete_meal(self):
        url = reverse("diet-detail", kwargs={'pk': self.diet.id})
        self.client.force_authenticate(user=self.user)
        response = self.client.delete(url, format="json")

        assert response.status_code == status.HTTP_204_NO_CONTENT

    def test_delete_meal_when_not_owner(self):
        not_owner = User.objects.create_user("not_owner@example.com", "secret")

        url = reverse("diet-detail", kwargs={'pk': self.diet.id})
        self.client.force_authenticate(user=not_owner)
        response = self.client.delete(url, format="json")

        assert response.status_code == status.HTTP_404_NOT_FOUND
