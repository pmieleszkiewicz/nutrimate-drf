# Nutrimate (Nutrition App)

RESTful API which allows user to manage diets. Diet consists of meals, products are part of meals. 
It's a try to rewrite my [Laravel app](https://gitlab.com/pmieleszkiewicz/nutrimate) using Django 3 with Django Rest Framework.

## Demo
Application is deployed on
[Heroku](https://nutrimate-django.herokuapp.com/api/).

Front-end application is avaiable [here](https://gitlab.com/pmieleszkiewicz/nutrimate-vue)

Login credentials:
```
Email: user@example.com
Password: secret_password
```


## Installation
```bash
pipenv shell
pipenv install
python manage.py migrate
python manage.py runserver
```

## Tests
```bash
python manage.py test
```

## Database schema
Simplified - removed Django tables.

![database schema](https://files.tinypic.pl/i/01003/v95w4lckx3hi.png)


## Todo
- add Factory Boy package and use it in tests
