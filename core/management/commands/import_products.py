import json

from django.core.management.base import BaseCommand, CommandError
from core.models import Product
from nutrimate import settings


class Command(BaseCommand):
    help = 'Imports products from .json file'

    def handle(self, *args, **options):
        base_dir = settings.BASE_DIR
        file_path = f"{base_dir}/core/management/commands/json/products.json"

        with open(file_path) as products:
            data = json.load(products)
            for product in data:
                Product.objects.get_or_create(
                    name=product['name'],
                    proteins=product['proteins'],
                    carbs=product['carbs'],
                    fats=product['fats'],
                    calories=product['calories']
                )
                self.stdout.write(self.style.SUCCESS(f"Added product {product['name']}!"))

            self.stdout.write(self.style.SUCCESS('Successfully imported products from .json file'))
