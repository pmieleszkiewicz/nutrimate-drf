from rest_framework import serializers

from .models import Product, Meal, MealHasProduct, Diet


class ProductSerializer(serializers.ModelSerializer):
    """
    Serializer used to serialize product instance
    """
    class Meta:
        model = Product
        fields = ['id', 'name', 'proteins', 'carbs', 'fats', 'calories']


class MealHasProductSerializer(serializers.ModelSerializer):
    """
    Serializer used to get additional data about products which belong to meal,
    such as macros and quantity of product (in grams).
    """
    id = serializers.ReadOnlyField(source='product.id')
    name = serializers.ReadOnlyField(source='product.name')
    proteins = serializers.SerializerMethodField()
    carbs = serializers.SerializerMethodField()
    fats = serializers.SerializerMethodField()
    calories = serializers.SerializerMethodField()

    class Meta:
        model = MealHasProduct
        fields = ['id', 'name', 'quantity', 'proteins', 'carbs', 'fats', 'calories']

    def get_proteins(self, obj):
        proteins = (obj.quantity * obj.product.proteins) / 100
        return round(proteins, 1)

    def get_carbs(self, obj):
        carbs = (obj.quantity * obj.product.carbs) / 100
        return round(carbs, 1)

    def get_fats(self, obj):
        fats = (obj.quantity * obj.product.fats) / 100
        return round(fats, 1)

    def get_calories(self, obj):
        calories = (obj.quantity * obj.product.calories) / 100
        return round(calories)


class MealSerializer(serializers.ModelSerializer):
    """
    Serializer used to retrieve meal.
    It serializes related products (through MealHasProduct) and diet.
    """
    products = MealHasProductSerializer(source='mealhasproduct_set', many=True, read_only=True)

    class Meta:
        model = Meal
        fields = ['id', 'name', 'diet', 'products']


class MealCreateSerializer(serializers.ModelSerializer):
    """
    Serializer used to create and update meal.
    The difference between MealSerializer is that it wants diet value as pk/id
    """
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Meal
        fields = ['id', 'name', 'user', 'diet']


class DietWithMealsSerializer(serializers.ModelSerializer):
    meals = MealSerializer(many=True, read_only=True)

    class Meta:
        model = Diet
        fields = ['id', 'name', 'description', 'meals', 'proteins', 'carbs', 'fats', 'calories', 'date_created']


class DietSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Diet
        fields = ['id', 'name', 'description', 'user', 'proteins', 'carbs', 'fats', 'calories']
