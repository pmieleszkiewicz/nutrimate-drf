from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, mixins, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .filters import ProductFilter
from .mixins import MultiSerializerViewSetMixin
from .models import Product, Meal, Diet, MealHasProduct
from .permissions import IsOwner
from .serializers import (
    ProductSerializer,
    MealSerializer,
    MealCreateSerializer,
    DietSerializer,
    DietWithMealsSerializer
)


class ProductViewSet(mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = (IsAuthenticated,)
    filter_class = ProductFilter


class MealViewSet(MultiSerializerViewSetMixin,
                  mixins.CreateModelMixin,
                  mixins.UpdateModelMixin,
                  mixins.DestroyModelMixin,
                  mixins.RetrieveModelMixin,
                  viewsets.GenericViewSet):
    queryset = Meal.objects.none()
    serializer_class = MealSerializer
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_action_classes = {
        'retrieve': MealSerializer,
        'update': MealCreateSerializer,
        'partial_update': MealCreateSerializer,
        'create': MealCreateSerializer
    }

    def get_queryset(self):
        if self.action == "retrieve":
            return Meal.objects.filter(user=self.request.user).prefetch_related("mealhasproduct_set__product")
        return Meal.objects.filter(user=self.request.user)

    @action(detail=True, methods=['patch'], url_path='update-product-quantity')
    def update_product_quantity(self, request, pk=None):
        meal = self.get_object()
        try:
            meal_product = meal.mealhasproduct_set.get(product=request.data.get('product_id'))
        except MealHasProduct.DoesNotExist:
            raise Http404
        meal_product.quantity = request.data.get('quantity')
        meal_product.save()

        serializer = MealSerializer(meal)
        return Response(serializer.data)

    @action(detail=True, methods=['delete'], url_path='remove-product')
    def remove_product(self, request, pk=None):
        meal = self.get_object()
        try:
            product = meal.products.get(pk=request.data.get('product_id'))
        except Product.DoesNotExist:
            raise Http404
        meal.products.remove(product)

        return Response(None, status=status.HTTP_204_NO_CONTENT)

    @action(detail=True, methods=['post'], url_path='add-product')
    def add_product(self, request, pk=None):
        meal = self.get_object()
        product = get_object_or_404(Product, pk=request.data.get('product_id'))
        meal.products.add(product, through_defaults={'quantity': 100})

        serializer = MealSerializer(meal)

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class DietViewSet(MultiSerializerViewSetMixin,
                  viewsets.ModelViewSet):
    queryset = Diet.objects.none()
    serializer_class = DietSerializer
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_action_classes = {
        'retrieve': DietWithMealsSerializer
    }

    def get_queryset(self):
        # eager load related meals and products (m2m relation)
        if self.action == "retrieve":
            return Diet.objects.filter(user=self.request.user).prefetch_related("meals__mealhasproduct_set__product")
        return Diet.objects.filter(user=self.request.user)
