from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model

from core.models import Product

User = get_user_model()


class ProductTests(APITestCase):
    def setUp(self) -> None:
        self.user = User.objects.create_user("test@example.com", "secret")

    """
    List meal tests
    [GET] /api/products/
    """
    def test_list_product_when_not_authenticated(self):
        url = reverse("product-list")
        response = self.client.get(url, format="json")

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_list_product_when_authenticated(self):
        Product.objects.create(name='Banana', proteins=0.2, carbs=23, fats=0, calories=92)

        url = reverse("product-list")
        self.client.force_authenticate(user=self.user)
        response = self.client.get(url, format="json")
        data = response.data['results']

        assert response.status_code == status.HTTP_200_OK
        assert len(data) == 1

    def test_list_product_filter_by_name(self):
        Product.objects.create(name='Banana', proteins=0.2, carbs=23, fats=0, calories=92)
        Product.objects.create(name='Banana cheesecake', proteins=0.2, carbs=23, fats=0, calories=92)
        Product.objects.create(name='Something banana', proteins=0.2, carbs=23, fats=0, calories=92)
        Product.objects.create(name='White rice', proteins=6.8, carbs=70, fats=0, calories=92)

        url = reverse("product-list")
        self.client.force_authenticate(user=self.user)
        response = self.client.get(url, {'name': 'banana'}, format="json")
        data = response.data['results']

        assert response.status_code == status.HTTP_200_OK
        assert len(data) == 3

    """
    Retrieve meal tests
    [GET] /api/meals/<pk>
    """
    def test_retrieve_product_when_not_authenticated(self):
        product = Product.objects.create(name='Banana', proteins=0.2, carbs=23, fats=0, calories=92)

        url = reverse("product-detail", kwargs={"pk": product.pk})
        response = self.client.get(url, format="json")

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_retrieve_product_when_authenticated(self):
        product = Product.objects.create(name='Banana', proteins=0.2, carbs=23, fats=0, calories=92)

        url = reverse("product-detail", kwargs={"pk": product.pk})
        self.client.force_authenticate(user=self.user)
        response = self.client.get(url, format="json")

        assert response.status_code == status.HTTP_200_OK
        assert response.data['name'] == product.name
