from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import api

router = DefaultRouter()
router.register('products', api.ProductViewSet)
router.register('meals', api.MealViewSet)
router.register('diets', api.DietViewSet)

urlpatterns = [
    path('', include(router.urls)),
]