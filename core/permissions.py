from rest_framework.permissions import BasePermission


class IsOwner(BasePermission):
    """
    Model must have 'user' field.
    """
    def has_object_permission(self, request, view, obj):
        return request.user == obj.user

