from django.contrib import admin

from .models import Product, Meal, MealHasProduct, Diet

admin.site.register(Product)
admin.site.register(Meal)
admin.site.register(MealHasProduct)
admin.site.register(Diet)
