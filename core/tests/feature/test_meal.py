from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model

from core.models import Meal, Diet

User = get_user_model()


class MealTests(APITestCase):
    def setUp(self) -> None:
        self.user = User.objects.create_user("test@example.com", "secret")
        self.diet = Diet.objects.create(name='Diet', user=self.user, proteins=150, carbs=300, fats=100, calories=2700)
        self.meal = Meal.objects.create(name='Breakfast', user=self.user, diet=self.diet)

    """
    Create meal tests
    [POST] /api/meals/
    """
    def test_create_meal_when_not_authenticated(self):
        url = reverse("meal-list")
        data = {}
        response = self.client.post(url, data, format="json")

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_create_meal_invalid_data(self):
        url = reverse("meal-list")
        data = {}
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, data, format="json")

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        self.assertIn('diet', response.data)
        self.assertIn('name', response.data)

    def test_create_meal_valid_data(self):
        url = reverse("meal-list")
        data = {'name': 'Breakfast', 'diet': self.diet.id}
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, data, format="json")

        assert response.status_code == status.HTTP_201_CREATED

    """
    Update meal tests
    [PUT] /api/meals/<id>
    """
    def test_put_meal_when_not_authenticated(self):
        url = reverse("meal-detail", args={'pk': self.meal.id})
        data = {}
        response = self.client.put(url, data, format="json")

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_put_meal_invalid_data(self):
        url = reverse("meal-detail", kwargs={'pk': self.meal.id})
        data = {}
        self.client.force_authenticate(user=self.user)
        response = self.client.put(url, data, format="json")

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        self.assertIn('name', response.data)
        self.assertIn('diet', response.data)

    def test_put_meal_valid_data(self):
        url = reverse("meal-detail", kwargs={'pk': self.meal.id})
        data = {
            'name': 'Dinner',
            'diet': self.diet.id
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.put(url, data, format="json")

        assert response.status_code == status.HTTP_200_OK
        assert response.data['name'] == 'Dinner'

    def test_put_meal_when_not_owner(self):
        not_owner = User.objects.create_user("not_owner@example.com", "secret")

        url = reverse("meal-detail", kwargs={'pk': self.meal.id})
        data = {
            'name': 'Dinner',
            'diet': self.diet.id
        }
        self.client.force_authenticate(user=not_owner)
        response = self.client.put(url, data, format="json")

        assert response.status_code == status.HTTP_404_NOT_FOUND

    """
    Update meal tests
    [PATCH] /api/meals/<id>
    """
    def test_patch_meal_when_not_authenticated(self):
        url = reverse("meal-detail", args={'pk': self.meal.id})
        data = {}
        response = self.client.patch(url, data, format="json")

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_patch_meal_valid_data(self):
        url = reverse("meal-detail", kwargs={'pk': self.meal.id})
        data = {
            'name': 'Dinner',
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.patch(url, data, format="json")

        assert response.status_code == status.HTTP_200_OK
        assert response.data['name'] == 'Dinner'

    def test_patch_meal_when_not_owner(self):
        not_owner = User.objects.create_user("not_owner@example.com", "secret")

        url = reverse("meal-detail", kwargs={'pk': self.meal.id})
        data = {
            'name': 'Dinner',
            'diet': self.diet.id
        }
        self.client.force_authenticate(user=not_owner)
        response = self.client.patch(url, data, format="json")

        assert response.status_code == status.HTTP_404_NOT_FOUND

    """
    Retrieve meal tests
    [GET] /api/meals/<id>
    """

    def retrieve_meal_when_not_authenticated(self):
        url = reverse("meal-detail", args={'pk': self.meal.id})
        response = self.client.get(url, format="json")

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_retrieve_meal_valid_data(self):
        url = reverse("meal-detail", kwargs={'pk': self.meal.id})
        self.client.force_authenticate(user=self.user)
        response = self.client.patch(url, format="json")

        assert response.status_code == status.HTTP_200_OK

    def test_retrieve_meal_when_not_owner(self):
        not_owner = User.objects.create_user("not_owner@example.com", "secret")

        url = reverse("meal-detail", kwargs={'pk': self.meal.id})
        self.client.force_authenticate(user=not_owner)
        response = self.client.patch(url, format="json")

        assert response.status_code == status.HTTP_404_NOT_FOUND

    """
    Delete meal tests
    [DELETE] /api/meals/<id>
    """

    def test_delete_meal_when_not_authenticated(self):
        url = reverse("meal-detail", args={'pk': self.meal.id})
        response = self.client.delete(url, format="json")

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_delete_meal(self):
        url = reverse("meal-detail", kwargs={'pk': self.meal.id})
        self.client.force_authenticate(user=self.user)
        response = self.client.delete(url, format="json")

        assert response.status_code == status.HTTP_204_NO_CONTENT

    def test_delete_meal_when_not_owner(self):
        not_owner = User.objects.create_user("not_owner@example.com", "secret")

        url = reverse("meal-detail", kwargs={'pk': self.meal.id})
        self.client.force_authenticate(user=not_owner)
        response = self.client.delete(url, format="json")

        assert response.status_code == status.HTTP_404_NOT_FOUND
